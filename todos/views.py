from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from .models import TodoList
from .forms import TodoListForm
from .forms import TodoItemForm
from .models import TodoItem

# Create your views here.
#I did a class model instead of writing a fx for the ToDoListView
class TodoListView (ListView):
    model = TodoList
    template_name = "todo_list.html"
    context_object_name = "todo_lists"


def todo_list_detail(request,id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "Todo_List": todo_list,
    }
    print(context)
    return render(request, "todo_list_detail.html",context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail.html", id= todo_list.id)

    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request,"todo_list_create.html",context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail.html", id= todo_item.list.id)

    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request,"todo_list_create.html",context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
        context = {
        "form": form,
    }
    return render(request, "todo_list_update.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.todo_list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    return render(
        request, "todo_item_update.html", {"form": form, "todo_item": todo_item}
    )
